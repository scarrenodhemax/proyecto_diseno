/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.unab.vina.diseno.refactory;

import cl.vina.dhemax.transantiago.dao.EquiposDAO;
import cl.vina.dhemax.transantiago.dto.Equipo;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sebastian
 */
public class Utilidades {
    
    // Configuracion de Utilidades
    public static final int ENCABEZADO = 0x55;
    
    // Tipos de Equipos
    public static final int PPM05 = 17;
    public static final int MIL_MODBUS = 12;
    public static final int AVC2 = 21;

    
    // Estados de Equipo
    
    public static final int CONECTADO = 3;
    public static final int DESCONECTADO = 99;
    public static final int FALLA_RED = 98;
    
    
    // CODIGO DE COMANDO
    public static final int CONSULTA_MEDIDAS = 84;
    public static final int CONSULTA_ESTADO = 85;
    public static final int EVENTO_DESDE_CARGADOR = 83;
    public static final int HEARTBEAT = 82;
    public static final int DIRECTIVA_CARGADOR = 86;
    public static final int CORRIENTE_FUGA = 87;
    public static final int INFORME_PFI = 88;
    public static final int EVENTO_CARGADOR = 92;
    public static final int ESTADOS_CAN = 106;
    public static final int VIN_CAN = 115;
    public static final int GPS_CAN = 116;
    public static final int MEDICIONES_CAN = 113;
    public static final int ALARMAS_CAN = 114;
//    public static final int DATOS_CAN = 115;

    public static final int ERROR_CONSULTAR_ENERGIA_MEDIDOR = 212;
    public static final int ERROR_CONSULTA_ESTADO = 213;
    public static final int ERROR_EVENTO_DESDE_CARGADOR = 211;
    public static final int ERROR_HEARTBEAT = 210;
    public static final int ERROR_DIRECTIVA_CARGADOR = 214;
    public static final int ERROR_CORRIENTE_FUGA = 215;
    public static final int ERROR_INFORME_PFI = 216;
    
    
    //Tipos de Datos
    //Estado
    public static final int AC = 27;
    public static final int AV = 28;
    public static final int BAUD_RATE = 29;
    public static final int WORK_STATUS = 30;
    public static final int ESTADO_CARGADOR = 39;
    public static final int SOC = 38;
    //Medidas PPM05
    public static final int BOCA_INHABILITADA = 47;
    public static final int ENERGIA_ACTIVA_A = 48;
    public static final int ENERGIA_ACTIVA_B = 49;
    public static final int ENERGIA_ACTIVA_C = 50;
    public static final int ENERGIA_APARENTE_A = 53;
    public static final int ENERGIA_APARENTE_B = 54;
    public static final int ENERGIA_APARENTE_C = 55;
//    public static final int VOLTAJE_AN = 7;
    public static final int CORRIENTE_N = 68;
    public static final int CORRIENTE_GND = 69;
    public static final int FRECUENCIA_A = 58;
    public static final int FRECUENCIA_B = 59;
    public static final int FRECUENCIA_C = 60;
    public static final int FACTOR_POTENCIA_A = 63;
    public static final int FACTOR_POTENCIA_B = 64;
    public static final int FACTOR_POTENCIA_C = 65;
    
    //Medidas MIL_MODBUS
    public static final int FACTOR_ESCALA_E = 6; //si
    public static final int ENERGIA_ACTIVA = 1; //
    public static final int ENERGIA_REACTIVA = 2;
    public static final int POTENCIA_ACTIVA_TOTAL = 3;  
    public static final int POTENCIA_REACTIVA_TOTAL = 4;
    public static final int FACTOR_POTENCIA = 5;
    public static final int VOLTAJE_AN = 7;
    public static final int VOLTAJE_BN = 8;
    public static final int VOLTAJE_CN = 9;
    public static final int CORRIENTE_A = 10;
    public static final int CORRIENTE_B = 11;
    public static final int CORRIENTE_C = 12;
    public static final int THD_VOLTAJE_FN_AN = 13;
    public static final int THD_VOLTAJE_FN_BN = 14;
    public static final int THD_VOLTAJE_FN_CN = 15;
    public static final int THD_CORRIENTE_F_A = 16;
    public static final int THD_CORRIENTE_F_B = 17;
    public static final int THD_CORRIENTE_F_C = 18;
    public static final int POTENCIA_ACTIVA_F_A = 19;
    public static final int POTENCIA_ACTIVA_F_B = 20;
    public static final int POTENCIA_ACTIVA_F_C = 21;
    public static final int POTENCIA_REACTIVA_F_A = 22;
    public static final int POTENCIA_REACTIVA_F_B = 23;
    public static final int POTENCIA_REACTIVA_F_C = 24;
    //Medidas PPM05
    
    
    
    // Tipo de dato EVENTOS
    public static final int CAMBIO_ESTADO_CARGADOR = 2;
    public static final int ID_CAMBIO_ESTADO_CARGADOR = 40; //mismo
    public static final int ALARMA_CARGADOR = 1;
    public static final int ID_ALARMA_CARGADOR = 124;
    public static final int FALLA_CARGADOR = 125;
    
    // Tipo de dato BUS CAN
    public static final int ODOMETRO = 1;
    public static final int ID_ODOMETRO = 70;
    public static final int TRIP = 2;
    public static final int ID_TRIP = 71;
    public static final int VELOCIDAD_VEHICULO = 3;
    public static final int ID_VELOCIDAD_VEHICULO = 72;
    public static final int ESTADO_CARGA = 4;
    public static final int ID_ESTADO_CARGA = 73;
    public static final int VOLTAJE_CELDA_MAS_ALTA = 5;
    public static final int ID_VOLTAJE_CELDA_MAS_ALTA = 74;
    public static final int VOLTAJE_CELDA_MAS_BAJA = 6;
    public static final int ID_VOLTAJE_CELDA_MAS_BAJA = 75;
    public static final int TEMPERATURA_CELDA_MAS_ALTA = 7;
    public static final int ID_TEMPERATURA_CELDA_MAS_ALTA = 76;
    public static final int TEMPERATURA_CELDA_MAS_BAJA = 8;
    public static final int ID_TEMPERATURA_CELDA_MAS_BAJA = 77;
    public static final int CAPACIDAD_CARGA = 9;
    public static final int ID_CAPACIDAD_CARGA = 78;
    public static final int CAPACIDAD_DESCARGA = 16;
    public static final int ID_CAPACIDAD_DESCARGA = 79;
    public static final int TIEMPO_CARGA = 17;
    public static final int ID_TIEMPO_CARGA = 80;
    public static final int TIEMPO_SETEADA_AIRE_ACONDICIONADO = 18;
    public static final int ID_TIEMPO_SETEADA_AIRE_ACONDICIONADO = 81;
    public static final int TEMPERATURA_CADA_BATERIA = 19;
    public static final int ID_TEMPERATURA_CADA_BATERIA = 82;
    public static final int VOLTAJE_CADA_BATERIA = 20;
    public static final int ID_VOLTAJE_CADA_BATERIA = 83;
    public static final int TEMPERATURA_CONTROLADOR_MOTOR_IZQUIERDO = 22;
    public static final int ID_TEMPERATURA_CONTROLADOR_MOTOR_IZQUIERDO = 84;
    public static final int VELOCIDAD_ROTACION_MOTOR_IZQUIERDO = 23;
    public static final int ID_VELOCIDAD_ROTACION_MOTOR_IZQUIERDO = 85;
    public static final int TEMPERATURA_MOTOR_IZQUIERDO = 24;
    public static final int ID_TEMPERATURA_MOTOR_IZQUIERDO = 86;
    public static final int TEMPERATURA_CONTROLADOR_MOTOR_DERECHO = 25;
    public static final int ID_TEMPERATURA_CONTROLADOR_MOTOR_DERECHO = 87;
    public static final int VELOCIDAD_ROTACION_MOTOR_DERECHO = 32;
    public static final int ID_VELOCIDAD_ROTACION_MOTOR_DERECHO = 88;
    public static final int TEMPERATURA_MOTOR_DERECHO = 33;
    public static final int ID_TEMPERATURA_MOTOR_DERECHO = 89;
    public static final int TEMPERATURA_EN_BUS = 34;
    public static final int ID_TEMPERATURA_EN_BUS = 90;
    public static final int TEMPERATURA_FUERA_BUS = 35;
    public static final int ID_TEMPERATURA_FUERA_BUS = 91;
    public static final int POSICION_PEDAL_ACELERACION = 36;
    public static final int ID_POSICION_PEDAL_ACELERACION = 92;
    public static final int CORRIENTE_MOTOR_IZQUIERDO = 37;
    public static final int ID_CORRIENTE_MOTOR_IZQUIERDO = 93;
    public static final int CORRIENTE_MOTOR_DERECHO = 38;
    public static final int ID_CORRIENTE_MOTOR_DERECHO = 94;
    
    // Tipo de dato BUS CAN
    public static final int POSICION_MARCHA = 1;
    public static final int ID_POSICION_MARCHA = 95;
    public static final int ESTADO_FUNCIONAMIENTO_CARGA_IZQUIERDA = 2;
    public static final int ID_ESTADO_FUNCIONAMIENTO_CARGA_IZQUIERDA = 96;
    public static final int ESTADO_CONEXION_CARGA_IZQUIERDA = 3;
    public static final int ID_ESTADO_CONEXION_CARGA_IZQUIERDA = 97;
    public static final int ESTADO_FALLA_CARGA_IZQUIERDA = 4;
    public static final int ID_ESTADO_FALLA_CARGA_IZQUIERDA = 98;
    public static final int ESTADO_FUNCIONAMIENTO_CARGA_DERECHA = 5;
    public static final int ID_ESTADO_FUNCIONAMIENTO_CARGA_DERECHA = 99;
    public static final int ESTADO_CONEXION_CARGA_DERECHA = 6;
    public static final int ID_ESTADO_CONEXION_CARGA_DERECHA = 100;
    public static final int ESTADO_FALLA_CARGA_DERECHA = 7;
    public static final int ID_ESTADO_FALLA_CARGA_DERECHA = 101;
    public static final int LUCES_BAJAS = 8;
    public static final int ID_LUCES_BAJAS = 102;
    public static final int LUZ_NEBLINERA = 9;
    public static final int ID_LUZ_NEBLINERA = 103;
    public static final int BOCINA = 16;
    public static final int ID_BOCINA = 104;
    public static final int LUZ_DIRECCION_DERECHA = 17;
    public static final int ID_LUZ_DIRECCION_DERECHA = 105;
    public static final int LUZ_DIRECCION_IZQUIERDA = 18;
    public static final int ID_LUZ_DIRECCION_IZQUIERDA = 106;
    public static final int LUCES_ALTAS = 19;
    public static final int ID_LUCES_ALTAS = 107;
    public static final int LUZ_FOCALIZADA = 20;
    public static final int ID_LUZ_FOCALIZADA = 108;
    public static final int ESTADO_PUERTA_A = 21;
    public static final int ID_ESTADO_PUERTA_A = 109;
    public static final int ESTADO_PUERTA_B = 22;
    public static final int ID_ESTADO_PUERTA_B = 110;
    public static final int ESTADO_AIRE_ACONDICIONADO = 23;
    public static final int ID_ESTADO_AIRE_ACONDICIONADO = 111;
    public static final int ESTADO_FUNCIONAMIENTO_GESTION_TERMICA_BATERIA = 24;
    public static final int ID_ESTADO_FUNCIONAMIENTO_GESTION_TERMICA_BATERIA = 112;
    public static final int ESTADO_PEDAL_FRENO = 25;
    public static final int ID_ESTADO_PEDAL_FRENO = 113;
    public static final int SENAL_FRENO_DE_MANO = 32;
    public static final int ID_SENAL_FRENO_DE_MANO = 114;
    public static final int LUCES_LATERALES = 33;
    public static final int ID_LUCES_LATERALES = 115;
    public static final int LUZ_ANTINIEBLA_TRASERA = 34;
    public static final int ID_LUZ_ANTINIEBLA_TRASERA = 116;
    public static final int ALARMA_VELOCIDAD_VEHICULO = 1;
    public static final int ID_ALARMA_VELOCIDAD_VEHICULO = 117;
    public static final int ALARMA_TEMPERATURA_REFRIGERANTE_A = 2;
    public static final int ID_ALARMA_TEMPERATURA_REFRIGERANTE_A = 118;
    public static final int ALARMA_TEMPERATURA_REFRIGERANTE_B = 3;
    public static final int ID_ALARMA_TEMPERATURA_REFRIGERANTE_B = 119;
    public static final int LUZ_DE_ALERTA_AMARILLA_ABS_EBS = 4;
    public static final int ID_LUZ_DE_ALERTA_AMARILLA_ABS_EBS = 120;
    public static final int ALARMA_FALLA_DIRECCION_MOTOR = 5;
    public static final int ID_ALARMA_FALLA_DIRECCION_MOTOR = 121;
    public static final int ALARMA_FALLA_MOTOR_COMPRESOR_AIRE = 6;
    public static final int ID_ALARMA_FALLA_MOTOR_COMPRESOR_AIRE = 122;
    public static final int FALLA_SISTEMA_FRENOS = 7;
    public static final int ID_FALLA_SISTEMA_FRENOS = 123;
    public static final int GPS = 126;
    public static final int VOLTAJE_CElDA_A = 12345;
    public static final int ID_VOLTAJE_CElDA_A = 95;
    public static final int VOLTAJE_CElDA_B = 12346;
    public static final int ID_VOLTAJE_CElDA_B = 96;
    public static final int VOLTAJE_CElDA_C = 12347;
    public static final int ID_VOLTAJE_CElDA_C = 97;
    
    /**
    * @author Sebastian
    * Funcionalidad: lista donde se guardaran todos los equipos del sistema
    * al comenzar el programa
    */
    public static List<Equipo> listaEquipos = new ArrayList<>();
    EquiposDAO equiposDAO = new EquiposDAO();
    
    /**
    * @author Sebastian
    * Funcionalidad: permite obtener el nombre de equipo dado su identificador
    */
    public static String getNombreEquipo(int id){
        String equipo = "";
        for (Equipo e : listaEquipos) {
            if(e.getId() == id){
                equipo = e.getNombre();
                break;
            }
        }
        return equipo;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite agregar un byte de validacion de tramas (CheckSum)
    */
    public static byte[] agregarCheckSum(byte[] cmdAux) {

        byte checkSum = 0;

        try {

            for (int i = 0; i < cmdAux.length; i++) {

                if (i == 0) {
                    checkSum = (byte) (cmdAux[i] ^ cmdAux[i + 1]);
                    i = i + 1;
                } else {
                    checkSum = (byte) (checkSum ^ cmdAux[i]);
                }

            }

            cmdAux[cmdAux.length - 1] = checkSum;

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return cmdAux;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite borrar bytes que sobran al final de la trama (bytes en 0)
    */
    public static byte[] clearBytes(byte[] byteData) {

        byte[] aux = null;

        try {

            byteData = separarDesdeEncabezado(byteData);

            int largo = (int) byteData[2] & 0xFF;
            aux = new byte[4 + largo];

            for (int i = 0; i < byteData.length; i++) {
                if (i < (largo + 4)) {
                    aux[i] = byteData[i];
                } else {
                    break;
                }
            }

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return aux;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: separa la data a partir de el flag de encabezado
    */
    private static byte[] separarDesdeEncabezado(byte[] arreglo) {

        byte[] arregloClear = new byte[arreglo.length];
        int posicion = 0;

        try {

            for (int i = 0; i < arreglo.length; i++) {
                if ((i + 1) <= arreglo.length) {
                    if ((arreglo[i] == 0x55) && (arreglo[i + 1] == 0x55)) {
                        posicion = i;
                        break;
                    }
                }
            }

            int j = 0;
            for (int i = posicion; i < arreglo.length; i++) {
                arregloClear[j] = arreglo[i];
                j++;
            }

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return arregloClear;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: transforma un arreglo de bytes en un string hexadecimal
    */
    public static String byteArrayToHexa(byte[] array) {

        String hexa = "";
        ByteArrayInputStream input = new ByteArrayInputStream(array);
        String aux;

        int leidos = input.read();

        while (leidos != -1) {
            aux = Integer.toHexString(leidos);
            if (aux.length() < 2) {
                hexa += "0";
            }

            hexa += aux;
            leidos = input.read();
        }

        return hexa;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: transforma un arreglo de bytes a string hexa, pero sin espacios
    */
    public static String byteArrayToStringSinEspacios(byte[] array) {
        String hexa = "";
        ByteArrayInputStream input = new ByteArrayInputStream(array);
        String aux;

        int leidos = input.read();

        while (leidos != -1) {
            aux = Integer.toHexString(leidos);
            if (aux.length() < 2) {
                hexa += "0";
            }
            hexa += aux;
            leidos = input.read();
        }
        return hexa;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: da vuelta un string que deberia representar un hexadecimal
    * para lugar poder transformarlas en bytes y devolverlas
    */
    public static String darVuelta(String string) {
        String strfinal = "";

        try {

            String[] auxArray = new String[(string.length() / 2)];

            int j = 0;

            for (int i = 0; i < string.length(); i += 2) {
                auxArray[(string.length() / 2) - (j + 1)] = string.substring(i, i + 2);

                j++;
            }

            for (int i = 0; i < auxArray.length; i++) {
                strfinal = strfinal + auxArray[i];
            }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        return strfinal;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: transforma un string que represente un hexadecimal en Entero de 16 bytes
    */
    public static BigInteger hexToBigInteger(String hex) {

        BigInteger bi = null;

        try {

            bi = new BigInteger(hex, 16);

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return bi;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: transforma un booleano a byte
    */
    public static byte boolToByte(boolean estado) {

        byte estadoByte = 0;

        try {

            if (estado) {
                estadoByte = 0x01;
            } else {
                estadoByte = 0x00;
            }

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return estadoByte;

    }
    
    /**
    * @author Sebastian
    * Funcionalidad: transforma un short a un arreglo de bytes
    */
    public static byte[] shortToBytes(short value) {
        byte[] returnByteArray = new byte[2];
        returnByteArray[0] = (byte) (value & 0xff);
        returnByteArray[1] = (byte) ((value >>> 8) & 0xff);
        return returnByteArray;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: genera el mod de un entero
    */
    public static int setModIdTask(int id) {
        int idMod = -1;
        try {
            if (id < 10000) {
                idMod = id;
            } else if (id >= 10000) {
                idMod = id % 10000;
            }
            return idMod;
        } catch (Exception ex) {
            System.out.println("ERROR " + ex.toString());
            return -1;
        }
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: le agrega un entero a una trama de byte y la devuelve
    */
    public static byte[] agregarIdTarea(byte[] cmd, int id) {
        byte[] aux = new byte[2];
        try {

            String hex = Integer.toHexString(id);

            if (hex.length() == 1) {
                aux[0] = (byte) Integer.parseInt(hex.substring(0, 1), 16);
                aux[1] = 0x00;
            } else if (hex.length() == 2) {
                if (Integer.parseInt(hex.substring(1, 2), 16) <= 0xf) {
                    aux[0] = (byte) Integer.parseInt(hex.substring(0, 2), 16);
                    aux[1] = 0x00;
                } else {
                    aux[1] = (byte) Integer.parseInt(hex.substring(1, 2), 16);
                    aux[0] = 0x00;
                }
            } else if (hex.length() == 3) {
                aux[0] = (byte) Integer.parseInt(hex.substring(1, 3), 16);
                aux[1] = (byte) Integer.parseInt(hex.substring(0, 1), 16);
            } else if (hex.length() == 4) {
                aux[0] = (byte) Integer.parseInt(hex.substring(2, 4), 16);
                aux[1] = (byte) Integer.parseInt(hex.substring(0, 2), 16);
            }

            cmd[4] = aux[0];
            cmd[5] = aux[1];

            return cmd;
        } catch (Exception ex) {
            System.out.println(ex.toString());
            return new byte[0];
        }
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite transformar un string en un arreglo de byte
    */
    public static byte[] hexToByteArray(String hex) {
        byte rc[] = new byte[hex.length() / 2];
        for (int i = 0; i < rc.length; i++) {
            String h = hex.substring(i * 2, i * 2 + 2);
            int x = Integer.parseInt(h, 16);
            rc[i] = (byte) x;
        }
        return rc;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite transformar un long en un arreglo de bytes
    */
    public static byte[] longToByteArray(Long p1) {

        long param = p1.longValue();
        int MASK = 0xff;
        byte[] result = new byte[4];
        for (int i = 0; i < 4; i++) {
            int offset = (result.length - 1 - i) * 8;
            result[3 - i] = (byte) ((param >>> offset) & MASK);
        }
        return result;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: le agrega el checksum a un arreglo de byte y lo devuelve
    */
    public static byte[] getCmdWithChecksum(byte[] cmdAux) {

        byte checkSum = 0;

        try {

            for (int i = 0; i < cmdAux.length; i++) {

                if (i == 0) {
                    checkSum = (byte) (cmdAux[i] ^ cmdAux[i + 1]);
                    i = i + 1;
                } else {
                    checkSum = (byte) (checkSum ^ cmdAux[i]);
                }

            }

            cmdAux[cmdAux.length - 1] = checkSum;

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return cmdAux;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite validar si el checksum de la trama es correcto
    */
    public static boolean isCheckSumValid(String cmd) {

        boolean isValid = false;

        try {

            byte[] cmdAComparar = hexToByteArray(cmd);
            byte checkSumOrginal = cmdAComparar[cmdAComparar.length - 1];

            cmdAComparar[cmdAComparar.length - 1] = 0x00;

            byte[] checkSumCalculado = getCmdWithChecksum(cmdAComparar);

            if (checkSumOrginal == checkSumCalculado[checkSumCalculado.length - 1]) {
                isValid = true;
            }

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return isValid;

    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite transformar un string que representa un hexadecimal
    * a un arreglo de byte
    */
    public static byte[] hexToByteArray(String hex) {
        byte rc[] = new byte[hex.length() / 2];
        for (int i = 0; i < rc.length; i++) {
            String h = hex.substring(i * 2, i * 2 + 2);
            int x = Integer.parseInt(h, 16);
            rc[i] = (byte) x;
        }
        return rc;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite obtener obtener el checksum de un arreglo de byte
    * y la devuelve con el checksum
    */
    public static byte[] getCmdWithChecksum(byte[] cmdAux) {

        byte checkSum = 0;

        try {

            for (int i = 0; i < cmdAux.length; i++) {

                if (i == 0) {
                    checkSum = (byte) (cmdAux[i] ^ cmdAux[i + 1]);
                    i = i + 1;
                } else {
                    checkSum = (byte) (checkSum ^ cmdAux[i]);
                }

            }

            cmdAux[cmdAux.length - 1] = checkSum;

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return cmdAux;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite cortar numeros decimales de un flotante
    */
    public static float cortarDecimales(float num, int cantDecimales) {

        try {

            if (String.valueOf(num).contains(".")) {

                String entero = String.valueOf(num).split("\\.")[0];
                String decimales = String.valueOf(num).split("\\.")[1];

                if (decimales.length() > cantDecimales) {
                    num = Float.parseFloat(entero + "." + decimales.substring(0, cantDecimales));
                }
            }

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return num;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite pasar de un string que simula un hexadecimal
    * a un flotante
    */
    public static float hexToFloat(String hex) {
        return bytesToFloat(hexToByteArray(hex));
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite pasar un arreglo de bytes a un flotante
    */
    public static float bytesToFloat(byte[] bytes) {
        int intBits = 0;
        for (int i = 0; i < 4; i++) {
            intBits |= (bytes[i] & 0xff) << (8 * i);
        }
        return (Float.intBitsToFloat(intBits));
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite pasar un string que simula un hexadecimal a un
    * timestamp unix
    */
    public static Timestamp hexToTimestamp(String hex) {

        Timestamp fecha = null;

        try {

            fecha = new Timestamp((long) Utilidades.hexToLong(hex) * 1000L);

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return fecha;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite pasar un string que simula un hexadecimal a un long
    */
    public static long hexToLong(String hex) {

        long numLong = 0;

        try {

            numLong = Long.parseLong(hex, 16);

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        return numLong;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite pasar un numero en forma de string a un ascii
    */
    public static String numberToAscii(String numero){
        String ascii ="";
        for (int i = 0; i < numero.length()/2 ; i++) {
            if(i==0){
                
                ascii = Character.toString((char) (Integer.parseInt(Utilidades.darVuelta(numero.substring(i, i+2)), 16)));
            }else{
                ascii += Character.toString((char) (Integer.parseInt(Utilidades.darVuelta(numero.substring(i*2, (i*2)+2)), 16)));
            }
        }
        return ascii;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite convertir de dos string que representen la parte
    * entera y la otra decimal de un numero a flotante
    */
    public static float convertirEnterosAFlotantes(String tramaEntera, String tramaDecimal) {
        String trama;
        trama = Integer.parseInt(Utilidades.darVuelta(tramaEntera), 16)+"."+Integer.parseInt(Utilidades.darVuelta(tramaDecimal), 16);
        float resultado = Float.valueOf(trama);
        return resultado;
    }

    /**
    * @author Sebastian
    * Funcionalidad: dado un entero que representa un codigo de comando
    * devuelve el nombre en string
    */
    public static String getTareaFullName(int codTarea) {

        String fullName = "";

        try {
            switch (codTarea) {

                case INFORME_PFI:
                    fullName = "INFORME PFI";
                    break;
                case CORRIENTE_FUGA:
                    fullName = "CORRIENTE DE FUGA";
                    break;
                case DIRECTIVA_CARGADOR:
                    fullName = "DIRECTIVA A CARGADOR";
                    break;
                case CONSULTA_ESTADO:
                    fullName = "CONSULTA ESTADO";
                    break;
                case CONSULTA_MEDIDAS:
                    fullName = "CONSULTAR ENERGIA MEDIDOR";
                    break;
                case EVENTO_DESDE_CARGADOR:
                    fullName = "EVENTO DESDE CARGADOR";
                    break;
                case HEARTBEAT:
                    fullName = "HEARTBEAT";
                    break;
                case ERROR_CONSULTAR_ENERGIA_MEDIDOR:
                    fullName = "ERROR CONSULTAR ENERGIA MEDIDOR";
                    break;
                case ERROR_CONSULTA_ESTADO:
                    fullName = "ERROR CONSULTA ESTADO";
                    break;
                case ERROR_EVENTO_DESDE_CARGADOR:
                    fullName = "ERROR EVENTO DESDE CARGADOR";
                    break;
                case ERROR_HEARTBEAT:
                    fullName = "ERROR HEARTBEAT";
                    break;
                case ERROR_DIRECTIVA_CARGADOR:
                    fullName = "ERROR DIRECTIVA CARGADOR";
                    break;
                case ERROR_CORRIENTE_FUGA:
                    fullName = "ERROR CORRIENTE FUGA";
                    break;
                case ERROR_INFORME_PFI:
                    fullName = "ERROR INFORME PFI";
                    break;
                default:
                    fullName = "COMANDO DESCONOCIDO :" + codTarea;
                    break;

            }
        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return fullName;
    }
    
}
