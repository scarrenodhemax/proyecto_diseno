/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.unab.vina.diseno.refactory;

import cl.vina.dhemax.transantiago.dao.DatosDAO;
import cl.vina.dhemax.transantiago.dao.EquiposDAO;
import cl.vina.dhemax.transantiago.dto.Dato;
import cl.vina.dhemax.transantiago.dto.Equipo;
import cl.vina.dhemax.transantiago.dto.PeriodoComando;
import cl.vina.dhemax.transantiago.utilidades.Log;
import static cl.unab.vina.diseno.refactory.Utilidades.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MSI
 */
public class PPM05 {
    
    //Consulta Estado
    private Integer idBcc; //id485
    private Double ac; //corriente cargador
    private Double av; //voltaje cargador
    private Integer baudRate; // velocidad cargador
    private Integer workStatus; // estado del cargador
    private Integer statusEvent; // evento de estado
    private Integer soc; // porcentaje de bateria
    
    //Consulta dato evento
    private Integer dato;
    
    //Consulta Medidas
    private Integer bocasInhabilitadas;
    private Float energiaActivaA;
    private Float energiaAparenteA;
    private Float voltajeA;
    private Float corrienteA;
    private Float frecuenciaA;
    private Integer factorPotenciaA;
    
    private Float energiaActivaB;
    private Float energiaAparenteB;
    private Float voltajeB;
    private Float corrienteB;
    private Float frecuenciaB;
    private Integer factorPotenciaB;
    
    private Float energiaActivaC;
    private Float energiaAparenteC;
    private Float voltajeC;
    private Float corrienteC;
    private Float frecuenciaC;
    private Integer factorPotenciaC;
    
    private Float corrienteN;
    private Float corrienteGND;

    
    

    private Timestamp timeStamp;
    
    //Inicio DatosDAO
    private DatosDAO datosDAO = new DatosDAO();
    EquiposDAO equipoDAO = new EquiposDAO();
    
    /**
    * @author Sebastian
    * Funcionalidad: permite clasificar un comando entrante, para luego enviarlo
    * al metodo correspondiente que es capaz de obter sus datos
    */
    public void clasificarComando(String comando, Integer codigoComando, Integer idRegistroMensajeEntrante, int idEquipo ){
        switch(codigoComando){
            
            case CONSULTA_ESTADO:
                this.asignarDatosConsultaEstado(comando, idRegistroMensajeEntrante, idEquipo);
                break;
            case CONSULTA_MEDIDAS:
                this.asignarDatosConsultaMedidas(comando, idRegistroMensajeEntrante, idEquipo);
                break;
            case EVENTO_DESDE_CARGADOR:
                this.clasificarDatos(comando, idRegistroMensajeEntrante, idEquipo);
                break;
            case EVENTO_CARGADOR:
                this.clasificarEventosCargador(comando, idRegistroMensajeEntrante, idEquipo);
                break;
                
            default:
                Log.write("COMANDO NO RECONOCIDO = "+comando);
                
        }
    }

    /**
    * @author Sebastian
    * Funcionalidad: permite recibir el comando Consulta Estados
    * y agregar estos datos a la base de datos
    */
    private void asignarDatosConsultaEstado(String comando, Integer idRegistroMensajeEntrante, int idEquipo ) {
        
        this.ac = (Integer.parseInt(Utilidades.darVuelta(comando.substring(26, 30)), 16) * 0.1) - 500;
        this.av = (double)Integer.parseInt(Utilidades.darVuelta(comando.substring(30, 34)), 16);
        this.baudRate = Integer.parseInt(Utilidades.darVuelta(comando.substring(34, 38)), 16);
        this.workStatus = Integer.parseInt(Utilidades.darVuelta(comando.substring(38, 40)), 16);
        this.soc = validarExisteSOC(Integer.parseInt(Utilidades.darVuelta(comando.substring(48, 50)), 16));
        
        List<Dato> listaDeDatos = new ArrayList<Dato>();
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, ac.toString(), Utilidades.AC));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, av.toString(), Utilidades.AV));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, baudRate.toString(), Utilidades.BAUD_RATE));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, workStatus.toString(), Utilidades.WORK_STATUS)); // Estado Caargador
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, workStatus.toString(), Utilidades.ID_CAMBIO_ESTADO_CARGADOR)); //Estado Visible
        equipoDAO.actualizarEstado(workStatus, idEquipo);
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, soc.toString(), Utilidades.SOC));
        datosDAO.insertarDatos(listaDeDatos);
        Log.write("[INGRESO][CONSULTA_ESTADO][EQUIPO:"+idEquipo+"][NOMBRE_EQUIPO: "+getNombreEquipo(idEquipo)+"][COMANDO]:"+comando);
    }
    

    /**
    * @author Sebastian
    * Funcionalidad: Esta validacion fue un parche si el valor entregado es 0-3 
    * implica que el bus no esta cargando o bien el dato no corresponde al SOC
    */
    private int validarExisteSOC(int soc){
        if (soc > 3){
            return soc;
        }
        return 0;
    }


    /**
    * @author Sebastian
    * Funcionalidad: Se clasifican los datos enviados por un equipo, el cual se 
    * debe evaliar a que tipo de dato pertenece, debido a que los del equipo
    * de HW tienen Tipos de Datos distintos a los indicados en la base de datos
    */
    private void clasificarDatos(String comando, Integer idRegistroMensajeEntrante, int idEquipo) {
        this.statusEvent = Integer.parseInt(Utilidades.darVuelta(comando.substring(22, 24)), 16);
        this.dato = Integer.parseInt(Utilidades.darVuelta(comando.substring(24, 26)), 16);
        switch(this.statusEvent){
            
            case ALARMA_CARGADOR:
                
                break;
                
            case CAMBIO_ESTADO_CARGADOR:
                datosDAO.insertarDatoEvento(this.dato.toString(), idRegistroMensajeEntrante, ID_CAMBIO_ESTADO_CARGADOR);
                this.comparadorCambioEstadoConWorkStatus(this.dato);
                datosDAO.insertarDatoEvento(this.dato.toString(), idRegistroMensajeEntrante, ESTADO_CARGADOR);
                equipoDAO.actualizarEstado(this.dato, idEquipo); // Actualizar estado Equipo
                Log.write("[INGRESO][EVENTO_DESDE_CARGADOR][EQUIPO:"+idEquipo+"][NOMBRE_EQUIPO: "+getNombreEquipo(idEquipo)+"][COMANDO]:"+comando);
                break;
                
            
        }
        
    }
    
    //Nuevo formato
    private void clasificarEventosCargador(String comando, Integer idRegistroMensajeEntrante, int idEquipo) {
        this.statusEvent = Integer.parseInt(Utilidades.darVuelta(comando.substring(22, 26)), 16);
        int largoDelDato = Integer.parseInt(Utilidades.darVuelta(comando.substring(26, 28)), 16);
        this.dato = Integer.parseInt(Utilidades.darVuelta(comando.substring(28, 28+(largoDelDato*2))), 16);
        datosDAO.insertarDatoEvento(this.dato.toString(), idRegistroMensajeEntrante, statusEvent);
        if(statusEvent == 40){
            this.comparadorCambioEstadoConWorkStatus(this.dato);
            datosDAO.insertarDatoEvento(this.dato.toString(), idRegistroMensajeEntrante, ESTADO_CARGADOR);
            equipoDAO.actualizarEstado(this.dato, idEquipo); // Actualizar estado Equipo
        }
        Log.write("[INGRESO][EVENTO_CARGADOR][EQUIPO:"+idEquipo+"][NOMBRE_EQUIPO: "+getNombreEquipo(idEquipo)+"][COMANDO]:"+comando);
            
        
    }
    

    /**
    * @author Sebastian
    * Funcionalidad: Adapta un numero de estado del firmware a un numero de
    * estado que sea de software y coincida en la base de datos
    */
     private int comparadorCambioEstadoConWorkStatus(int dato){
        switch(dato){
                    
                    case 0:
                        this.dato = 6;
                        break;
                        
                    case 1:
                        this.dato = 5;
                        break;
                        
                    case 2:
                        this.dato = 2;
                        break;
                        
                    case 3:
                        this.dato = 3;
                        break;
                        
                    case 4:
                        this.dato = 4;
                        break;
                        
                    case 5:
                        this.dato = 26;
                        break;
                        
                    case 6:
                        this.dato = 6;
                        break;
                        
                    case 7:
                        this.dato = 6;
                        break;
                    default:
                        Log.write("[ERROR] ESTADO NO VALIDO -->"+dato);
                }
        return this.dato;
    }
    
     /**
    * @author Sebastian
    * Funcionalidad: permite crear el comando Consulta de Estado
    */
    public byte[] armarComandoConsultaEstado(PeriodoComando periodoComando, Equipo equipo) {

        byte[] cmd = null;
        try {
            byte largo;
            byte numCmd;
            largo = 0x08;
            numCmd = 0x55;
            byte[] idMedidorByteArray = this.getIdEquipo(equipo);

            byte[] cmdAux = {
                Utilidades.ENCABEZADO,
                Utilidades.ENCABEZADO,
                largo,
                numCmd,
                0x00,
                0x00,
                (byte)equipo.getIdTipoEquipo(),
                idMedidorByteArray[0],
                idMedidorByteArray[1],
                idMedidorByteArray[2],
                idMedidorByteArray[3],
                0x00
            };

            cmd = cmdAux;

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return cmd;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: Permite crear el comando Consulta Medidas
    */
    public byte[] armarComandoConsultaMedidas(PeriodoComando periodoComando, Equipo equipo) {

        byte[] cmd = null;
        try {
            byte largo;
            byte numCmd;
            largo = 0x08;
            numCmd = 0x54;
            byte[] idMedidorByteArray = this.getIdEquipo(equipo);

            byte[] cmdAux = {
                Utilidades.ENCABEZADO,
                Utilidades.ENCABEZADO,
                largo,
                numCmd,
                0x00,
                0x00,
                (byte)equipo.getIdTipoEquipo(),
                idMedidorByteArray[0],
                idMedidorByteArray[1],
                idMedidorByteArray[2],
                idMedidorByteArray[3],
                0x00
            };

            cmd = cmdAux;

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return cmd;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: transforma un atributo de un Objeto Equipo y lo pasa a
    * un arreglo de bytes
    */
    private byte[] getIdEquipo(Equipo equipo) {

        byte[] idMedidorAux = new byte[4];

        try {

            idMedidorAux = Utilidades.longToByteArray((long) equipo.getIdentificadorEquipo());

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return idMedidorAux;
    }

    /**
    * @author Sebastian
    * Funcionalidad: permite recibir el Consulta Medidas
    * y agregar estos datos a la base de datos
    */
    private void asignarDatosConsultaMedidas(String comando, Integer idRegistroMensajeEntrante, int idEquipo) {
        //Se obtiene antes el factor escala para poder utilizarlo en la energia activa y reactiva
        
        this.bocasInhabilitadas = Integer.parseInt(Utilidades.darVuelta(comando.substring(26, 30)), 16);
        this.energiaActivaA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(30, 38)), 2));
        this.energiaAparenteA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(38, 46)), 2));
        this.voltajeA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(46, 54)), 2));
        this.corrienteA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(54, 62)), 2));
        this.frecuenciaA =  Utilidades.convertirEnterosAFlotantes(comando.substring(62, 64), comando.substring(64, 66));
        this.factorPotenciaA = Integer.parseInt(Utilidades.darVuelta(comando.substring(66, 68)), 16);
        this.energiaActivaB = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(68, 76)), 2));
        this.energiaAparenteB = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(76, 84)), 2));
        this.voltajeB = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(84, 92)), 2));
        this.corrienteB = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(92, 100)), 2));
        this.frecuenciaB = Utilidades.convertirEnterosAFlotantes(comando.substring(100, 102), comando.substring(102, 104));
        this.factorPotenciaB = Integer.parseInt(Utilidades.darVuelta(comando.substring(104, 106)), 16);
        this.energiaActivaC = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(106, 114)), 2));
        this.energiaAparenteC = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(114, 122)), 2));
        this.voltajeC = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(122, 130)), 2));
        this.corrienteC = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(130, 138)), 2));
        this.frecuenciaC = Utilidades.convertirEnterosAFlotantes(comando.substring(138, 140), comando.substring(140, 142));
        this.factorPotenciaC = Integer.parseInt(Utilidades.darVuelta(comando.substring(142, 144)), 16);
//        this.energiaActivaA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(142, 150)), 2));
//        this.energiaAparenteA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(150, 158)), 2));
//        this.voltajeA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(158, 166)), 2));
        this.corrienteN = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(168, 176)), 2));
//        this.frecuenciaA = Integer.parseInt(Utilidades.darVuelta(comando.substring(174, 178)), 16);
//        this.factorPotenciaA = Integer.parseInt(Utilidades.darVuelta(comando.substring(178, 180)), 16);
//        this.energiaActivaA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(180, 188)), 2));
//        this.energiaAparenteA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(188, 196)), 2));
//        this.voltajeA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(196, 204)), 2));
        this.corrienteGND = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(206, 214)), 2));
//        this.frecuenciaA = Integer.parseInt(Utilidades.darVuelta(comando.substring(212, 216)), 16);
//        this.factorPotenciaA = Integer.parseInt(Utilidades.darVuelta(comando.substring(216, 218)), 16);
        
        
        
        List<Dato> listaDeDatos = new ArrayList<Dato>();
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, bocasInhabilitadas.toString(), Utilidades.BOCA_INHABILITADA));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, energiaActivaA.toString(), Utilidades.ENERGIA_ACTIVA_A));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, energiaAparenteA.toString(), Utilidades.ENERGIA_APARENTE_A));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, voltajeA.toString(), Utilidades.VOLTAJE_AN));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, corrienteA.toString(), Utilidades.CORRIENTE_A));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, frecuenciaA.toString(), Utilidades.FRECUENCIA_A));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, factorPotenciaA.toString(), Utilidades.FACTOR_POTENCIA_A));
        
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, energiaActivaB.toString(), Utilidades.ENERGIA_ACTIVA_B));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, energiaAparenteB.toString(), Utilidades.ENERGIA_APARENTE_B));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, voltajeB.toString(), Utilidades.VOLTAJE_BN));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, corrienteB.toString(), Utilidades.CORRIENTE_B));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, frecuenciaB.toString(), Utilidades.FRECUENCIA_B));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, factorPotenciaB.toString(), Utilidades.FACTOR_POTENCIA_B));
        
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, energiaActivaC.toString(), Utilidades.ENERGIA_ACTIVA_C));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, energiaAparenteC.toString(), Utilidades.ENERGIA_APARENTE_C));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, voltajeC.toString(), Utilidades.VOLTAJE_CN));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, corrienteC.toString(), Utilidades.CORRIENTE_C));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, frecuenciaC.toString(), Utilidades.FRECUENCIA_C));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, factorPotenciaC.toString(), Utilidades.FACTOR_POTENCIA_C));
        

        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, corrienteN.toString(), Utilidades.CORRIENTE_N));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, corrienteGND.toString(), Utilidades.CORRIENTE_GND));


        datosDAO.insertarDatos(listaDeDatos);
        Log.write("[INGRESO][CONSULTA_MEDIDA][EQUIPO:"+idEquipo+"][NOMBRE_EQUIPO: "+getNombreEquipo(idEquipo)+"][COMANDO]:"+comando);
    }
    
    
    
    
    
}
