/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.unab.vina.diseno.refactory;

import cl.vina.dhemax.transantiago.dao.DatosDAO;
import cl.vina.dhemax.transantiago.dto.Dato;
import cl.vina.dhemax.transantiago.dto.Equipo;
import cl.vina.dhemax.transantiago.dto.PeriodoComando;
import cl.vina.dhemax.transantiago.utilidades.Log;
import static cl.unab.vina.diseno.refactory.Utilidades.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MSI
 */
public class MIL_MODBUS {
    
    //Consulta dato evento
    private Integer dato;
    
    //Consulta Medidas
    private Integer factorEscalaE;
    private Double energiaActiva;
    private Double energiaReactiva;
    private Float potenciaActivaTotal;
    private Float potenciaReactivaTotal;
    private Float factorPotencia;
    private Float voltajeAN;
    private Float voltajeBN;
    private Float voltajeCN;
    private Float corrienteA;
    private Float corrienteB;
    private Float corrienteC;
    private Float thdVoltajeFN_AN;
    private Float thdVoltajeFN_BN;
    private Float thdVoltajeFN_CN;
    private Float thdCorrienteF_A;
    private Float thdCorrienteF_B;
    private Float thdCorrienteF_C;
    private Float potenciaActivaF_A;
    private Float potenciaActivaF_B;
    private Float potenciaActivaF_C;
    private Float potenciaReactivaF_A;
    private Float potenciaReactivaF_B;
    private Float potenciaReactivaF_C;
    private Timestamp timeStamp;
    
    //Inicio DatosDAO
    private DatosDAO datosDAO = new DatosDAO();
    
    
    /**
    * @author Sebastian
    * Funcionalidad: permite clasificar un comando entrante, para luego enviarlo
    * al metodo correspondiente que es capaz de obter sus datos
    */
    public void clasificarComando(String comando, Integer codigoComando, Integer idRegistroMensajeEntrante, int idEquipo ){
        switch(codigoComando){
            
            case CONSULTA_MEDIDAS:
                this.asignarDatosConsultaMedidas(comando, idRegistroMensajeEntrante, idEquipo);
                break;
        }
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite recibir el comando Consulta Medidas
    * y agregar estos datos a la base de datos
    */
    private void asignarDatosConsultaMedidas(String comando, Integer idRegistroMensajeEntrante, int idEquipo) {
        //Se obtiene antes el factor escala para poder utilizarlo en la energia activa y reactiva
        this.factorEscalaE = (new Integer((short) Integer.parseInt(Utilidades.darVuelta(comando.substring(64, 68)), 16)));

        this.energiaActiva = (Long.parseLong(Utilidades.darVuelta(comando.substring(28, 36)), 16) * Math.pow(10, this.factorEscalaE));
        this.energiaReactiva = (Long.parseLong(Utilidades.darVuelta(comando.substring(36, 44)), 16) * Math.pow(10, this.factorEscalaE));
        this.potenciaActivaTotal = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(44, 52)), 2));
        this.potenciaReactivaTotal = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(52, 60)), 2));
        this.factorPotencia = ((float) Integer.parseInt(Utilidades.darVuelta(comando.substring(60, 64)), 16) / 100);
        this.voltajeAN = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(68, 76)), 2));
        this.voltajeBN = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(76, 84)), 2));
        this.voltajeCN = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(84, 92)), 2));
        this.corrienteA = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(92, 100)), 2));
        this.corrienteB = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(100, 108)), 2));
        this.corrienteC = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(108, 116)), 2));
        this.thdVoltajeFN_AN = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(116, 124)), 2));
        this.thdVoltajeFN_BN = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(124, 132)), 2));
        this.thdVoltajeFN_CN = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(132, 140)), 2));
        this.thdCorrienteF_A = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(140, 148)), 2));
        this.thdCorrienteF_B = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(148, 156)), 2));
        this.thdCorrienteF_C = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(156, 164)), 2));
        this.potenciaActivaF_A = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(164, 172)), 2));
        this.potenciaActivaF_B = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(172, 180)), 2));
        this.potenciaActivaF_C = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(180, 188)), 2));
        this.potenciaReactivaF_A = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(188, 196)), 2));
        this.potenciaReactivaF_B = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(196, 204)), 2));
        this.potenciaReactivaF_C = (Utilidades.cortarDecimales(Utilidades.hexToFloat(comando.substring(204, 212)), 2));
        
        List<Dato> listaDeDatos = new ArrayList<Dato>();
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, energiaActiva.toString(), Utilidades.ENERGIA_ACTIVA));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, energiaReactiva.toString(), Utilidades.ENERGIA_REACTIVA));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, potenciaActivaTotal.toString(), Utilidades.POTENCIA_ACTIVA_TOTAL));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, potenciaReactivaTotal.toString(), Utilidades.POTENCIA_REACTIVA_TOTAL));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, factorPotencia.toString(), Utilidades.FACTOR_POTENCIA));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, voltajeAN.toString(), Utilidades.VOLTAJE_AN));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, voltajeBN.toString(), Utilidades.VOLTAJE_BN));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, voltajeCN.toString(), Utilidades.VOLTAJE_CN));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, corrienteA.toString(), Utilidades.CORRIENTE_A));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, corrienteB.toString(), Utilidades.CORRIENTE_B));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, corrienteC.toString(), Utilidades.CORRIENTE_C));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, thdVoltajeFN_AN.toString(), Utilidades.THD_VOLTAJE_FN_AN));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, thdVoltajeFN_BN.toString(), Utilidades.THD_VOLTAJE_FN_BN));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, thdVoltajeFN_CN.toString(), Utilidades.THD_VOLTAJE_FN_CN));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, thdCorrienteF_A.toString(), Utilidades.THD_CORRIENTE_F_A));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, thdCorrienteF_B.toString(), Utilidades.THD_CORRIENTE_F_B));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, thdCorrienteF_C.toString(), Utilidades.THD_CORRIENTE_F_C));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, potenciaActivaF_A.toString(), Utilidades.POTENCIA_ACTIVA_F_A));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, potenciaActivaF_B.toString(), Utilidades.POTENCIA_ACTIVA_F_B));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, potenciaActivaF_C.toString(), Utilidades.POTENCIA_ACTIVA_F_C));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, potenciaReactivaF_A.toString(), Utilidades.POTENCIA_REACTIVA_F_A));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, potenciaReactivaF_B.toString(), Utilidades.POTENCIA_REACTIVA_F_B));
        listaDeDatos.add(new Dato(idRegistroMensajeEntrante, potenciaReactivaF_C.toString(), Utilidades.POTENCIA_REACTIVA_F_C));
        datosDAO.insertarDatos(listaDeDatos);
        Log.write("[INGRESO][CONSULTA_ESTADO][EQUIPO:"+idEquipo+"][COMANDO]:"+comando);
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: permite crear el comando Consulta Medidas
    */
    public byte[] armarComandoConsultaMedidas(PeriodoComando periodoComando, Equipo equipo) {

        byte[] cmd = null;
        try {
            byte largo;
            byte numCmd;
            largo = 0x08;
            numCmd = 0x54;
            byte[] idMedidorByteArray = this.getIdEquipo(equipo);

            byte[] cmdAux = {
                Utilidades.ENCABEZADO,
                Utilidades.ENCABEZADO,
                largo,
                numCmd,
                0x00,
                0x00,
                (byte)equipo.getIdTipoEquipo(),
                idMedidorByteArray[0],
                idMedidorByteArray[1],
                idMedidorByteArray[2],
                idMedidorByteArray[3],
                0x00
            };

            cmd = cmdAux;

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return cmd;
    }
    
    /**
    * @author Sebastian
    * Funcionalidad: transforma un atributo de un Objeto Equipo y lo pasa a
    * un arreglo de bytes
    */
    private byte[] getIdEquipo(Equipo equipo) {

        byte[] idMedidorAux = new byte[4];

        try {

            idMedidorAux = Utilidades.longToByteArray((long) equipo.getIdentificadorEquipo());

        } catch (Exception ex) {
            Log.write(ex.toString());
        }

        return idMedidorAux;
    }
    
    
}
